package club.jitpcst.pcstinterface;

import club.jitpcst.pcstinterface.global.AdminInterceptor;
import club.jitpcst.pcstinterface.global.TokenInterceptor;
import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.convert.converter.Converter;
import org.springframework.web.filter.HttpPutFormContentFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocket;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 天宇
 */
@SpringBootApplication
@MapperScan("club.jitpcst.pcstinterface")
@EnableWebSocket
public class PcstInterfaceApplication implements WebMvcConfigurer {

    public static void main(String[] args) {
        SpringApplication.run(PcstInterfaceApplication.class, args);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new TokenInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/static/**", "/error/**")
                .excludePathPatterns("/users/login", "/users/register")
                .excludePathPatterns("/league-info")
                .excludePathPatterns("/activities/list", "/activities/get")
                .excludePathPatterns("/past-activities/list", "/past-activities/get")
                .excludePathPatterns("/messages/list", "/messages/get");

        registry.addInterceptor(new AdminInterceptor())
                .addPathPatterns("/users/list-all", "/users/list", "/users/delete")
                .addPathPatterns("/users/update-by-admin", "/users/insert", "/users/reset-password-by-admin")
                .addPathPatterns("/users/enable", "/users/disable")
                .addPathPatterns("/messages/delete", "/messages/update")
                .addPathPatterns("/activities/insert", "/activities/delete", "/activities/update")
                .addPathPatterns("/leagueInfo/update")
                .addPathPatterns("/past-activities/insert", "/past-activities/delete", "/past-activities/update");
    }

//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/**")
//                .allowCredentials(true)
//                .allowedOrigins("*")
//                .allowedMethods("*")
//                .allowedHeaders("*");
//    }

    /**
     * 支持put接收参数
     *
     * @return
     */
    @Bean
    public HttpPutFormContentFilter httpPutFormContentFilter() {
        return new HttpPutFormContentFilter();
    }

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
//        addResourceHandlers(registry);
    }

    /**
     * war包方式，也就是使用tomcat时，不需要这段。
     *
     * @return
     */
    /*@Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }*/
    @Bean
    public Converter<String, Date> addNewConvert() {
        return new Converter<String, Date>() {
            @Override
            public Date convert(String source) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = null;
                try {
                    date = sdf.parse(source);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return date;
            }
        };
    }

}
