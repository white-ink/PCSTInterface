package club.jitpcst.pcstinterface.controller;

import club.jitpcst.pcstinterface.entity.Ticket;
import club.jitpcst.pcstinterface.entity.User;
import club.jitpcst.pcstinterface.global.Constants;
import club.jitpcst.pcstinterface.service.TicketService;
import club.jitpcst.pcstinterface.service.UserService;
import club.jitpcst.pcstinterface.util.PassUtil;
import club.jitpcst.pcstinterface.util.TResult;
import club.jitpcst.pcstinterface.util.TResultCode;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.hibernate.validator.constraints.Length;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotEmpty;
import java.util.Arrays;
import java.util.Date;

/**
 * 普通controller，无论哪种角色，都能访问
 *
 * @author 天宇小凡
 * @since 2018-04-14
 */
@RestController
@RequestMapping("/users")
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private UserService userService;
    @Autowired
    private HttpSession session;
    @Autowired
    private TicketService ticketService;

    @PostMapping("login")
    public TResult login(@Validated @NotEmpty String username, @Validated @NotEmpty String password) {
        User user = userService.selectOne(new EntityWrapper<User>().eq("email", username).or().eq("phone", username));
        if (user == null || !user.getPwd().equals(PassUtil.getMD5(password))) {
            return TResult.failure(TResultCode.USER_LOGIN_ERROR);
        }
        // 此用户未审核
        if (user.getStatus() != User.STATUS_SUCCESS) {
            return TResult.failure(TResultCode.USER_ACCOUNT_FORBIDDEN);
        }
        String token = PassUtil.generatorToken(user.getUserId());
        Ticket ticket = new Ticket();
        ticket.setToken(token);
        ticket.setGmtCreate(new Date());
        ticket.setUserId(user.getUserId());
        if (ticketService.insertOrUpdate(ticket)) {
            return TResult.success(token);
        }
        return TResult.failure(TResultCode.BUSINESS_ERROR);
    }

    /**
     * 注册，头像可不传
     *
     * @param user
     * @return
     */
    @PostMapping("register")
    public TResult register(@Validated User user) {
        User dbUser = userService.selectOne(new EntityWrapper<User>().eq("phone", user.getPhone()).or().eq("email", user.getEmail()));
        if (dbUser != null) {
            return TResult.failure(TResultCode.DATA_ALREADY_EXISTED);
        }
        user.setGmtCreate(new Date());
        user.setGmtModified(new Date());
        user.setPwd(PassUtil.getMD5(user.getPwd()));
        user.setRole(User.ROLE_COMMON);
        user.setStatus(User.STATUS_NOT_SUCCESS);
        if (userService.insert(user)) {
            return TResult.success();
        }
        return TResult.failure(TResultCode.BUSINESS_ERROR);
    }

    @PutMapping("update-head-image")
    public TResult updateHeadImg(String newHeadImgUrl) {
        long userId = (long) session.getAttribute(Constants.SESSION_KEY_USER_ID);
        if (userService.updateForSet("head_img_url='" + newHeadImgUrl + "'", new EntityWrapper<User>().eq("user_id", userId))) {
            return TResult.success();
        }
        return TResult.failure(TResultCode.BUSINESS_ERROR);
    }

    /**
     * 获取用户自己的信息
     *
     * @return
     */
    @GetMapping
    public TResult getUserInfo() {
        long userId = (long) session.getAttribute(Constants.SESSION_KEY_USER_ID);
        User user = userService.selectById(userId);
        user.setPwd("");
        return TResult.success(user);
    }

    @RequestMapping
    public TResult logout() {
        long userId = (long) session.getAttribute(Constants.SESSION_KEY_USER_ID);
        session.removeAttribute(Constants.SESSION_KEY_USER_ID);
        session.invalidate();
        ticketService.delete(new EntityWrapper<Ticket>().eq("user_id", userId));
        return TResult.success();
    }

    /**
     * 管理员查看用户，不分页，用在穿梭框上
     *
     * @return
     */
    @GetMapping("list-all")
    public TResult listAllUserInfo() {
        return TResult.success(userService.selectList(new EntityWrapper<User>().setSqlSelect("user_id", "email", "phone", "real_name", "role", "status").orderBy("gmt_create", false)));
    }

    @DeleteMapping("delete")
    public TResult deleteUser(long id) {
        if (!userService.checkPermission((long) session.getAttribute(Constants.SESSION_KEY_USER_ID), id)) {
            return TResult.failure(TResultCode.PERMISSION_NO_ACCESS);
        }
        if (userService.deleteById(id)) {
            return TResult.success();
        }
        return TResult.failure(TResultCode.BUSINESS_ERROR);
    }

    /**
     * 用户修改自己的信息
     */
    @PutMapping("update")
    public TResult updateUserInfo(User user) {
        long userId = (long) session.getAttribute(Constants.SESSION_KEY_USER_ID);
        int count = userService.selectCount(new EntityWrapper<User>().eq("email", user.getEmail()).or().eq("phone", user.getPhone()).andNew().ne("user_id", userId));
        if (count > 0) {
            return TResult.failure(TResultCode.DATA_ALREADY_EXISTED);
        }
        User dbUser = userService.selectById(userId);
        dbUser.setEmail(user.getEmail());
        dbUser.setPhone(user.getPhone());
        dbUser.setRole(user.getRole());
        dbUser.setRealName(user.getRealName());
        dbUser.setGmtModified(new Date());
        dbUser.setStatus(user.getStatus());
        if (userService.updateById(dbUser)) {
            return TResult.success();
        }
        return TResult.failure(TResultCode.FAILURE);
    }

    /**
     * 管理员修改用户信息
     */
    @PutMapping("update-by-admin")
    public TResult updateUserInfoByAdmin(User user) {
        // 判断权限
        if (!userService.checkPermission((long) session.getAttribute(Constants.SESSION_KEY_USER_ID), user.getUserId())) {
            return TResult.failure(TResultCode.PERMISSION_NO_ACCESS);
        }

        User dbUser = userService.selectById(user.getUserId());
        if (dbUser == null) {
            return TResult.failure(TResultCode.USER_NOT_EXIST);
        }
        // 判断数据库的其他用户是否有相同手机号或邮箱
        int count = userService.selectCount(new EntityWrapper<User>().eq("email", user.getEmail()).or().eq("phone", user.getPhone()).andNew().ne("user_id", user.getUserId()));
        if (count > 0) {
            return TResult.failure(TResultCode.DATA_ALREADY_EXISTED);
        }
        dbUser.setEmail(user.getEmail());
        dbUser.setPhone(user.getPhone());
        dbUser.setRole(user.getRole());
        dbUser.setRealName(user.getRealName());
        dbUser.setGmtModified(new Date());
        dbUser.setStatus(user.getStatus());

        if (userService.updateById(dbUser)) {
            return TResult.success();
        }
        return TResult.failure(TResultCode.BUSINESS_ERROR);
    }

    @PostMapping("insert")
    public TResult insertUser(@Validated User user) {
        User dbUser = userService.selectOne(new EntityWrapper<User>().eq("phone", user.getPhone()).or().eq("email", user.getEmail()));
        if (dbUser != null) {
            return TResult.failure(TResultCode.DATA_ALREADY_EXISTED);
        }
        user.setUserId(null);
        user.setPwd(PassUtil.getMD5(user.getPwd()));
        user.setGmtModified(new Date());
        // 管理员添加的用户都设置为已经成功注册
        user.setStatus(User.STATUS_SUCCESS);
        user.setGmtCreate(new Date());
        if (userService.insert(user)) {
            return TResult.success();
        }
        return TResult.failure(TResultCode.BUSINESS_ERROR);
    }

    /**
     * 用户修改自己的密码
     *
     * @return
     */
    @PutMapping("reset-password")
    public TResult resetUserPassword(@Validated @NotEmpty String oldPassword, @Validated @NotEmpty @Length(min = 6) String newPassword) {
        long userId = (long) session.getAttribute(Constants.SESSION_KEY_USER_ID);
        User user = userService.selectById(userId);
        if (!user.getPwd().equals(PassUtil.getMD5(oldPassword))) {
            return TResult.failure("密码错误");
        }
        if (userService.updateForSet("pwd=" + PassUtil.getMD5(newPassword), new EntityWrapper<User>().eq("user_id", userId))) {
            return TResult.success();
        }
        return TResult.failure(TResultCode.BUSINESS_ERROR);
    }

    /**
     * 管理员重置用户密码
     *
     * @return
     */
    @PutMapping("reset-password-by-admin")
    public TResult resetUserPasswordByAdmin(long userId, @Validated @NotEmpty @Length(min = 6) String password) {
        String md5Password = PassUtil.getMD5(password);
        if (userService.updateForSet("pwd = '" + md5Password + "'", new EntityWrapper<User>().eq("user_id", userId))) {
            return TResult.success();
        }
        return TResult.failure(TResultCode.BUSINESS_ERROR);
    }

    /**
     * 根据真实姓名，分页查询用户
     *
     * @param realName
     * @param page
     * @return
     */
    @GetMapping("list")
    public TResult listUser(@RequestParam(required = false, defaultValue = "") String realName, Page<User> page) {
        return TResult.success(userService.selectPage(page, new EntityWrapper<User>().like("real_name", realName).setSqlSelect("user_id", "email", "phone", "real_name",
                "role", "status", "gmt_create", "gmt_modified").orderBy("gmt_create", false)));
    }

    /**
     * 允许注册用户登录
     *
     * @param usersId
     * @return
     */
    @PutMapping("enable")
    public TResult enableUsers(@RequestParam Long[] usersId) {
        if (userService.updateForSet("status=" + User.STATUS_SUCCESS, new EntityWrapper<User>().in("user_id", Arrays.asList(usersId)))) {
            return TResult.success();
        }
        return TResult.failure(TResultCode.BUSINESS_ERROR);
    }

    /**
     * 禁止注册登录
     *
     * @param usersId
     * @return
     */
    @PutMapping("disable")
    public TResult disableUsers(@RequestParam Long[] usersId) {
        if (userService.updateForSet("status=" + User.STATUS_NOT_SUCCESS, new EntityWrapper<User>().in("user_id", Arrays.asList(usersId)))) {
            return TResult.success();
        }
        return TResult.failure(TResultCode.BUSINESS_ERROR);
    }
}

