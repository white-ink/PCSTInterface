package club.jitpcst.pcstinterface.controller;

import club.jitpcst.pcstinterface.entity.LeagueInfo;
import club.jitpcst.pcstinterface.service.LeagueInfoService;
import club.jitpcst.pcstinterface.util.TResult;
import club.jitpcst.pcstinterface.util.TResultCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 社团介绍
 *
 * @author 天宇小凡
 * @since 2018-04-14
 */
@RestController
@RequestMapping("/league-info")
public class LeagueInfoController {

    @Autowired
    private LeagueInfoService leagueInfoService;

    @GetMapping
    public TResult getLeagueInfo() {
        return TResult.success(leagueInfoService.selectById(LeagueInfo.UNIQUE_ID));
    }

    @PutMapping("update")
    public TResult updateLeagueInfo(LeagueInfo leagueInfo) {
        leagueInfo.setId(LeagueInfo.UNIQUE_ID);
        if (leagueInfoService.updateById(leagueInfo)) {
            return TResult.success();
        }
        return TResult.failure(TResultCode.BUSINESS_ERROR);
    }
}