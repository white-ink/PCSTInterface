package club.jitpcst.pcstinterface.controller;


import club.jitpcst.pcstinterface.entity.Message;
import club.jitpcst.pcstinterface.entity.User;
import club.jitpcst.pcstinterface.global.Constants;
import club.jitpcst.pcstinterface.service.MessageService;
import club.jitpcst.pcstinterface.service.UserService;
import club.jitpcst.pcstinterface.util.TResult;
import club.jitpcst.pcstinterface.util.TResultCode;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author 天宇小凡
 * @since 2018-07-16
 */
@RestController
@RequestMapping("/messages")
public class MessageController {

    @Autowired
    private MessageService messageService;
    @Autowired
    private HttpSession session;
    @Autowired
    private UserService userService;

    @DeleteMapping("delete")
    public TResult deleteMessage(long messageId) {
        long userId = (long) session.getAttribute(Constants.SESSION_KEY_USER_ID);
        if (messageService.delete(new EntityWrapper<Message>().eq("message_id", messageId).eq("publisher", userId))) {
            return TResult.success();
        }
        return TResult.failure(TResultCode.FAILURE);
    }

    @PutMapping("update")
    public TResult updateMessage(Message message) {
        long userId = (long) session.getAttribute(Constants.SESSION_KEY_USER_ID);
        Message dbMessage = messageService.selectById(message.getMessageId());
        if (dbMessage == null || dbMessage.getPublisher() != userId) {
            return TResult.failure(TResultCode.RESULT_DATA_NONE);
        }
        dbMessage.setContent(message.getContent());
        dbMessage.setGmtModified(new Date());
        dbMessage.setRewardAmout(message.getRewardAmout());
        dbMessage.setTitle(message.getTitle());
        dbMessage.setType(message.getType());

        if (messageService.updateById(dbMessage)) {
            return TResult.success();
        }
        return TResult.failure(TResultCode.BUSINESS_ERROR);
    }

    @PostMapping("insert")
    public TResult insertMessage(@Validated Message message) {
        long userId = (long) session.getAttribute(Constants.SESSION_KEY_USER_ID);
        message.setGmtModified(new Date());
        message.setGmtCreate(new Date());
        message.setPublisher(userId);
        if (messageService.insert(message)) {
            return TResult.success();
        }
        return TResult.failure(TResultCode.BUSINESS_ERROR);
    }

    @GetMapping("get")
    public TResult getMessageById(@Validated @Min(1) long id) {
        return TResult.success(messageService.selectById(id));
    }

    /**
     * 根据标题查询
     *
     * @param title
     * @return
     */
    @GetMapping("list")
    public TResult searchMessageByTitle(@RequestParam(required = false) String title, Page<Message> page) {
        Page<Message> result = messageService.selectPage(page, new EntityWrapper<Message>().like("title", title).orderBy("gmt_create", false));
        if (result.getRecords().size() > 0) {
            List<Long> publishersId = new ArrayList<>();
            result.getRecords().forEach(u -> publishersId.add(u.getPublisher()));
            List<User> publisherUsers = userService.selectBatchIds(publishersId);
            result.getRecords().forEach(activity -> {
                for (User publisher : publisherUsers) {
                    if (activity.getPublisher().equals(publisher.getUserId())) {
                        activity.setPublisherName(publisher.getRealName());
                        activity.setPublisherHeadImg(publisher.getHeadImgUrl());
                        break;
                    }
                }
            });
        }
        return TResult.success(result);
    }
}