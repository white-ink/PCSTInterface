package club.jitpcst.pcstinterface.controller;

import club.jitpcst.pcstinterface.entity.Activity;
import club.jitpcst.pcstinterface.entity.PastActivity;
import club.jitpcst.pcstinterface.entity.User;
import club.jitpcst.pcstinterface.global.Constants;
import club.jitpcst.pcstinterface.service.ActivityService;
import club.jitpcst.pcstinterface.service.PastActivityService;
import club.jitpcst.pcstinterface.service.UserService;
import club.jitpcst.pcstinterface.util.TResult;
import club.jitpcst.pcstinterface.util.TResultCode;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Date;

/**
 * 活动总结 controller
 *
 * @author 天宇小凡
 * @since 2018-04-14
 */
@RestController
@RequestMapping("/past-activities")
public class PastActivityController {

    @Autowired
    private PastActivityService pastActivityService;
    @Autowired
    private HttpSession httpSession;
    @Autowired
    private UserService userService;
    @Autowired
    private ActivityService activityService;

    @GetMapping("list")
    public TResult listPastActivities(Page<PastActivity> page) {
        return TResult.success(pastActivityService.selectPage(page, new EntityWrapper<PastActivity>().orderBy("gmt_create", false)));
    }

    @DeleteMapping("delete")
    public TResult deletPasteActivity(long id) {
        if (pastActivityService.deleteById(id)) {
            return TResult.success();
        }
        return TResult.failure(TResultCode.BUSINESS_ERROR);
    }

    @PostMapping("insert")
    public TResult insertPastActivity(@Validated PastActivity pastActivity) {
        // 先将活动状态更新为结束状态
        activityService.updateForSet("status=" + Activity.STATUS_END, new EntityWrapper<Activity>().eq("activity_id", pastActivity.getActivityId()));

        long userId = (long) httpSession.getAttribute(Constants.SESSION_KEY_USER_ID);
        User user = userService.selectById(userId);
        pastActivity.setGmtCreate(new Date());
        pastActivity.setGmtModified(new Date());
        pastActivity.setEditor(user.getRealName());
        if (pastActivityService.insert(pastActivity)) {
            return TResult.success();
        }
        return TResult.failure(TResultCode.BUSINESS_ERROR);
    }

    @PutMapping("update")
    public TResult updatePastActivity(PastActivity pastActivity) {
        long userId = (long) httpSession.getAttribute(Constants.SESSION_KEY_USER_ID);
        User user = userService.selectById(userId);
        pastActivity.setGmtModified(new Date());
        pastActivity.setEditor(user.getRealName());
        if (pastActivityService.updateById(pastActivity)) {
            return TResult.success();
        }
        return TResult.failure(TResultCode.BUSINESS_ERROR);
    }

    @GetMapping("get")
    public TResult getPastActivityById(long id) {
        return TResult.success(pastActivityService.selectById(id));
    }
}

