package club.jitpcst.pcstinterface.controller;

import club.jitpcst.pcstinterface.global.QiniuConstant;
import club.jitpcst.pcstinterface.util.TResult;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("qiniu")
public class QiniuController {

    /**
     * 得到七牛上传文件的token
     */
    @GetMapping("token")
    public TResult getQiniuUploadToken() {
        UploadManager manager = new UploadManager();
        Auth auth = Auth.create(QiniuConstant.ACCESS_KEY, QiniuConstant.SECRET_KEY);
        String token = auth.uploadToken(QiniuConstant.BUCKET);
        return TResult.success(token);
    }
}
