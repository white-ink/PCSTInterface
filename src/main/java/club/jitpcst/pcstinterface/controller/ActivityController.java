package club.jitpcst.pcstinterface.controller;

import club.jitpcst.pcstinterface.entity.Activity;
import club.jitpcst.pcstinterface.entity.ActivityPartner;
import club.jitpcst.pcstinterface.entity.User;
import club.jitpcst.pcstinterface.global.Constants;
import club.jitpcst.pcstinterface.service.ActivityPartnerService;
import club.jitpcst.pcstinterface.service.ActivityService;
import club.jitpcst.pcstinterface.service.UserService;
import club.jitpcst.pcstinterface.util.TResult;
import club.jitpcst.pcstinterface.util.TResultCode;
import club.jitpcst.pcstinterface.websocket.TWebSocket;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 天宇小凡
 * @since 2018-04-14
 */
@RestController
@RequestMapping("/activities")
public class ActivityController {

    private final ActivityService activityService;
    private final HttpSession httpSession;
    @Autowired
    private ActivityPartnerService activityPartnerService;
    @Autowired
    private UserService userService;

    @Autowired
    public ActivityController(ActivityService activityService, HttpSession httpSession) {
        this.activityService = activityService;
        this.httpSession = httpSession;
    }

    @DeleteMapping("delete")
    public TResult deleteActivity(long id) {
        // 删除参与者
        activityPartnerService.delete(new EntityWrapper<ActivityPartner>().eq("activity_id", id));
        if (activityService.deleteById(id)) {
            return TResult.success();
        }
        return TResult.failure(TResultCode.BUSINESS_ERROR);
    }

    @PostMapping("insert")
    public TResult insertActivity(Activity activity) {
        System.out.println("开始时间：" + activity.getStartTime());
        long userId = (long) httpSession.getAttribute(Constants.SESSION_KEY_USER_ID);
        activity.setGmtCreate(new Date());
        activity.setGmtModified(new Date());
        activity.setPublisher(userId);
        activity.setStatus(Activity.STATUS_ONGOING);
        if (activityService.insert(activity)) {
            // 插入参与者
            activityService.insertActivityPartners(activity.getActivityId(), activity.getPartnersId());
            TWebSocket.sendMessageToAllUsers(Constants.MESSAGE);
            return TResult.success();
        }
        return TResult.failure(TResultCode.BUSINESS_ERROR);
    }

    @PutMapping("update")
    public TResult updateActivity(Activity activity) {
        activity.setGmtModified(new Date());

        if (activityService.updateById(activity)) {
            // 删除旧的参与者
            activityPartnerService.delete(new EntityWrapper<ActivityPartner>().eq("activity_id", activity.getActivityId()));
            // 添加新的参与者
            activityService.insertActivityPartners(activity.getActivityId(), activity.getPartnersId());
            return TResult.success();
        }
        return TResult.failure(TResultCode.BUSINESS_ERROR);
    }

    @GetMapping("get")
    public TResult getActivityById(long id) {
        List<ActivityPartner> activityPartners = activityPartnerService.selectList(new EntityWrapper<ActivityPartner>().eq("activity_id", id));
        Activity activity = activityService.selectById(id);

        // 取所有参与者id
        List<Long> partnersId = new ArrayList<>();
        activityPartners.forEach(activityPartner -> {
            partnersId.add(activityPartner.getUserId());
        });
        activity.setPartnersId(partnersId);
        // 获取参与者名字
        if (partnersId.size() > 0) {
            List<User> partners = userService.selectBatchIds(partnersId);
            activity.setPartnersName(partners.stream().map(User::getRealName).collect(Collectors.toList()));
        }
        User user = userService.selectById(activity.getPublisher());
        activity.setPublisherName(user.getRealName());
        return TResult.success(activity);
    }

    @GetMapping("list")
    public TResult searchActivitiesByTitle(@RequestParam(required = false) String title, Page<Activity> page) {
        Page<Activity> result = activityService.selectPage(page, new EntityWrapper<Activity>().like("title", title).orderBy("gmt_create", false));
        if (result.getRecords().size() > 0) {
            List<Long> publishersId = new ArrayList<>();
            result.getRecords().forEach(activity -> publishersId.add(activity.getPublisher()));
            List<User> publisherUsers = userService.selectBatchIds(publishersId);
            result.getRecords().forEach(activity -> {
                for (User publisher : publisherUsers) {
                    if (activity.getPublisher().equals(publisher.getUserId())) {
                        activity.setPublisherName(publisher.getRealName());
                        activity.setPublisherHeadImg(publisher.getHeadImgUrl());
                        break;
                    }
                }
            });
        }
        return TResult.success(result);
    }
}

