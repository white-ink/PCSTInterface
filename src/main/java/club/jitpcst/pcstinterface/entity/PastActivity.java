package club.jitpcst.pcstinterface.entity;

import com.baomidou.mybatisplus.annotations.TableId;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author 天宇小凡
 * @since 2018-04-14
 */
public class PastActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 往期活动，是与之前发布过的活动相关联的。
     */
    @TableId
    @NotNull(message = "activityId不能为空")
    @Min(value = 1, message = "请传入合法id")
    private Long activityId;
    @NotBlank(message = "标题不能为空")
    private String title;
    @NotBlank(message = "内容不能为空")
    private String content;
    /**
     * 展示在活动首页的图片
     */
    @NotBlank(message = "展示图片不能为空")
    private String displayImg;
    private String editor;
    private Date gmtCreate;
    private Date gmtModified;

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDisplayImg() {
        return displayImg;
    }

    public void setDisplayImg(String displayImg) {
        this.displayImg = displayImg;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    public String toString() {
        return "PastActivity{" +
                "activityId=" + activityId +
                ", title=" + title +
                ", content=" + content +
                ", displayImg=" + displayImg +
                ", editor=" + editor +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }
}
