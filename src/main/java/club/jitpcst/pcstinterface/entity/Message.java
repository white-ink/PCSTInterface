package club.jitpcst.pcstinterface.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author 天宇小凡
 * @since 2018-07-16
 */
@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
public class Message implements Serializable {

    public static final int TYPE_COMMON = 0;
    public static final int TYPE_HELP = 1;
    public static final int TYPE_REWARD = 2;
    /**
     * 新增功能
     */
    public static final int TYPE_FEATURE = 3;
    public static final int TYPE_BUG = 4;
    private static final long serialVersionUID = 1L;
    @TableId(value = "message_id", type = IdType.AUTO)
    private Long messageId;
    @NotEmpty
    private String title;
    @NotEmpty
    private String content;
    /**
     * 留言类型:0普通,1求助,2悬赏, 3新增功能，4bug反馈
     */
    @NotNull
    private Integer type;
    private Long publisher;
    /**
     * 悬赏金额，类型元
     */
    private Double rewardAmout;
    private Date gmtCreate;
    private Date gmtModified;

    @TableField(exist = false)
    private String publisherName;
    @TableField(exist = false)
    private String publisherHeadImg;

    public String getPublisherHeadImg() {
        return publisherHeadImg;
    }

    public void setPublisherHeadImg(String publisherHeadImg) {
        this.publisherHeadImg = publisherHeadImg;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public Long getPublisher() {
        return publisher;
    }

    public void setPublisher(Long publisher) {
        this.publisher = publisher;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Double getRewardAmout() {
        return rewardAmout;
    }

    public void setRewardAmout(Double rewardAmout) {
        this.rewardAmout = rewardAmout;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    public String toString() {
        return "Message{" +
                "messageId=" + messageId +
                ", title=" + title +
                ", content=" + content +
                ", type=" + type +
                ", rewardAmout=" + rewardAmout +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }
}
