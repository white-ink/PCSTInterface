package club.jitpcst.pcstinterface.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * 社团介绍
 *
 * @author 天宇小凡
 * @since 2018-04-14
 */
public class LeagueInfo implements Serializable {

    /**
     * 社团只有一条信息，id为1
     */
    public static final int UNIQUE_ID = 1;
    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String abbreviation;
    private String fullName;
    private String description;
    /**
     * 社团创建时间
     */
    private Date createTime;
    /**
     * 所属学院
     */
    private String college;
    /**
     * 社团负责人
     */
    private String manager;
    /**
     * 社团成员数
     */
    private Integer memberNum;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public Integer getMemberNum() {
        return memberNum;
    }

    public void setMemberNum(Integer memberNum) {
        this.memberNum = memberNum;
    }

    @Override
    public String toString() {
        return "LeagueInfo{" +
                "abbreviation=" + abbreviation +
                ", fullName=" + fullName +
                ", description=" + description +
                ", createTime=" + createTime +
                ", college=" + college +
                ", manager=" + manager +
                ", memberNum=" + memberNum +
                "}";
    }
}
