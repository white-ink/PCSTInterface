package club.jitpcst.pcstinterface.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author 天宇小凡
 * @since 2018-04-14
 */
public class Activity implements Serializable {

    /**
     * 活动进行中
     */
    public static final int STATUS_ONGOING = 0;
    /**
     * 活动已结束
     */
    public static final int STATUS_END = 1;
    private static final long serialVersionUID = 1L;
    @TableId(value = "activity_id", type = IdType.AUTO)
    private Long activityId;
    /**
     * 活动标题
     */
    private String title;
    /**
     * 内容
     */
    private String content;
    private Date startTime;
    private Date endTime;
    /**
     * 发布者的id
     */
    private Long publisher;
    @TableField(exist = false)
    private String publisherName;
    @TableField(exist = false)
    private String publisherHeadImg;
    /**
     * 活动状态，默认正在进行。到结束时间，后台会自动结束
     */
    private Integer status;
    private Date gmtCreate;
    private Date gmtModified;

    /**
     * 活动参与者id，接收前台的参数
     */
    @TableField(exist = false)
    private List<Long> partnersId;
    /**
     * 活动参与者，给前台传递参数
     */
    @TableField(exist = false)
    private List<String> partnersName;

    public String getPublisherHeadImg() {
        return publisherHeadImg;
    }

    public void setPublisherHeadImg(String publisherHeadImg) {
        this.publisherHeadImg = publisherHeadImg;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public List<String> getPartnersName() {
        return partnersName;
    }

    public void setPartnersName(List<String> partnersName) {
        this.partnersName = partnersName;
    }

    public List<Long> getPartnersId() {
        return partnersId;
    }

    public void setPartnersId(List<Long> partnersId) {
        this.partnersId = partnersId;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Long getPublisher() {
        return publisher;
    }

    public void setPublisher(Long publisher) {
        this.publisher = publisher;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "activityId=" + activityId +
                ", title=" + title +
                ", content=" + content +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", publisher=" + publisher +
                ", status=" + status +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }
}
