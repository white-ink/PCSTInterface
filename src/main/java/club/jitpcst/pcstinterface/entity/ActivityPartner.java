package club.jitpcst.pcstinterface.entity;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author 天宇小凡
 * @since 2018-04-14
 */
public class ActivityPartner implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 对应的activity
     */
    private Long activityId;
    /**
     * 对应的用户
     */
    private Long userId;

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "ActivityPartner{" +
                "activityId=" + activityId +
                ", userId=" + userId +
                "}";
    }
}
