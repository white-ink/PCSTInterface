package club.jitpcst.pcstinterface.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

/**
 * @author 天宇小凡
 * @since 2018-04-14
 */
public class User implements Serializable {

    /**
     * 普通用户
     */
    public static final int ROLE_COMMON = 0;
    /**
     * 副社长
     */
    public static final int ROLE_DEPUTY_HEAD = 1;
    /**
     * 社长
     */
    public static final int ROLE_PRESIDENT = 2;

    /**
     * 审核未通过
     */
    public static final int STATUS_NOT_SUCCESS = 0;
    /**
     * 审核通过
     */
    public static final int STATUS_SUCCESS = 1;

    private static final long serialVersionUID = 1L;

    @TableId(value = "user_id", type = IdType.AUTO)
    private Long userId;
    @NotEmpty
    @Email
    private String email;
    @NotEmpty
    private String phone;
    @NotEmpty
    @Length(min = 6)
    private String pwd;
    /**
     * 真实姓名
     */
    @NotEmpty
    private String realName;
    /**
     * 头像的base64编码
     */
    private String headImgUrl;
    /**
     * 用户角色：0普通社员，1:副社长，2:社长
     */
    private Integer role;
    /**
     * 用户状态：0:注册未审核，1:注册已通过
     */
    private Integer status;
    /**
     * 用户注册时间
     */
    private Date gmtCreate;
    /**
     * 最后修改时间
     */
    private Date gmtModified;

    public String getHeadImgUrl() {
        return headImgUrl;
    }

    public void setHeadImgUrl(String headImgUrl) {
        this.headImgUrl = headImgUrl;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", email=" + email +
                ", phone=" + phone +
                ", pwd=" + pwd +
                ", realName=" + realName +
                ", role=" + role +
                ", status=" + status +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }
}
