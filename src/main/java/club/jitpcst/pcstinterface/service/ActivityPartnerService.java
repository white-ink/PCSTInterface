package club.jitpcst.pcstinterface.service;

import club.jitpcst.pcstinterface.entity.ActivityPartner;
import club.jitpcst.pcstinterface.mapper.ActivityPartnerMapper;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 天宇小凡
 * @since 2018-04-14
 */
@Service
public class ActivityPartnerService extends ServiceImpl<ActivityPartnerMapper, ActivityPartner> implements IService<ActivityPartner> {

}
