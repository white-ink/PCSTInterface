package club.jitpcst.pcstinterface.service;

import club.jitpcst.pcstinterface.entity.LeagueInfo;
import club.jitpcst.pcstinterface.mapper.LeagueInfoMapper;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 天宇小凡
 * @since 2018-04-14
 */
@Service
public class LeagueInfoService extends ServiceImpl<LeagueInfoMapper, LeagueInfo> implements IService<LeagueInfo> {

}
