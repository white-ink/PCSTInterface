package club.jitpcst.pcstinterface.service;

import club.jitpcst.pcstinterface.entity.User;
import club.jitpcst.pcstinterface.mapper.UserMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 天宇小凡
 * @since 2018-04-14
 */
@Service
public class UserService extends ServiceImpl<UserMapper, User> {

    /**
     * 检查操作者的权限是否大于等于目标用户的权限，用来修改用户信息，禁用用户等。
     *
     * @param operatorUserId 操作者id
     * @param targetUserId   目标用户id
     * @return true/false
     */
    public boolean checkPermission(long operatorUserId, long targetUserId) {
        User currUser = this.selectById(operatorUserId);
        User targetUser = this.selectById(targetUserId);
        return currUser.getRole() >= targetUser.getRole();
    }
}
