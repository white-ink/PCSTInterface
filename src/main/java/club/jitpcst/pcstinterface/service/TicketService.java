package club.jitpcst.pcstinterface.service;

import club.jitpcst.pcstinterface.entity.Ticket;
import club.jitpcst.pcstinterface.global.Constants;
import club.jitpcst.pcstinterface.mapper.TicketMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 天宇小凡
 * @since 2018-04-14
 */
@Service
public class TicketService extends ServiceImpl<TicketMapper, Ticket> {

    public Long checkTokenAndGetUserId(String token) {
        Ticket ticket = selectOne(new EntityWrapper<Ticket>().eq("token", token));
        if (ticket != null && System.currentTimeMillis() - ticket.getGmtCreate().getTime() <= Constants.TOKEN_MAX_VALID_TIME) {
            return ticket.getUserId();
        }
        return null;
    }
}
