package club.jitpcst.pcstinterface.service;

import club.jitpcst.pcstinterface.entity.Message;
import club.jitpcst.pcstinterface.mapper.MessageMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author 天宇小凡
 * @since 2018-07-16
 */
@Service
public class MessageService extends ServiceImpl<MessageMapper, Message> {

}
