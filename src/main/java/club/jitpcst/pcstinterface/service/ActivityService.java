package club.jitpcst.pcstinterface.service;

import club.jitpcst.pcstinterface.entity.Activity;
import club.jitpcst.pcstinterface.entity.ActivityPartner;
import club.jitpcst.pcstinterface.mapper.ActivityMapper;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 天宇小凡
 * @since 2018-04-14
 */
@Service
public class ActivityService extends ServiceImpl<ActivityMapper, Activity> implements IService<Activity> {

    @Autowired
    private ActivityPartnerService activityPartnerService;

    public void insertActivityPartners(Long activityId, List<Long> partnersId) {
        List<ActivityPartner> activityPartners = new ArrayList<>(16);
        if (partnersId != null && partnersId.size() > 0) {
            for (Long partnerId : partnersId) {
                ActivityPartner partner = new ActivityPartner();
                partner.setActivityId(activityId);
                partner.setUserId(partnerId);
                activityPartners.add(partner);
            }
            activityPartnerService.insertBatch(activityPartners);
        }
    }
}
