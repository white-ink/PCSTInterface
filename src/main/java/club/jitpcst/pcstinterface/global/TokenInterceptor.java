package club.jitpcst.pcstinterface.global;

import club.jitpcst.pcstinterface.service.TicketService;
import club.jitpcst.pcstinterface.util.SpringBeanFactoryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * 全局拦截器，验证token
 *
 * @author 天宇小凡
 */
public class TokenInterceptor implements HandlerInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(TokenInterceptor.class);
    private TicketService ticketService = SpringBeanFactoryUtil.getBean(TicketService.class);

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse response, Object o) throws IOException {
//        response.setHeader("Access-Control-Allow-Origin","*");
//        response.setHeader("Access-Control-Allow-Credentials", "true");
//        response.setHeader("Access-Control-Allow-Methods", "*");
//        response.setHeader("Access-Control-Max-Age", "5000");
//        response.setHeader("Access-Control-Allow-Headers", "*");
        if (httpServletRequest.getMethod().equals(RequestMethod.OPTIONS.toString())) {
            LOGGER.debug("-----检查------");
            return true;
        }
        LOGGER.debug(httpServletRequest.getRequestURI());
        HttpSession httpSession = httpServletRequest.getSession();
        String token = httpServletRequest.getHeader(Constants.TOKEN);
        Long userId = ticketService.checkTokenAndGetUserId(token);
        if (userId != null) {
            httpSession.setAttribute(Constants.SESSION_KEY_USER_ID, userId);
            return true;
        }
        response.sendError(HttpServletResponse.SC_FORBIDDEN, "token is invalid");
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {

    }
}
