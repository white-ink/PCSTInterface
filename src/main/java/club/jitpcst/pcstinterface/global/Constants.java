package club.jitpcst.pcstinterface.global;

public class Constants {
    public static final String SECRET = "pcst_club_jit.2018";
    public static final String TOKEN = "token";
    public static final String TIMESTAMP = "timestamp";
    public static final String SIGN = "sign";
    public static final int TIMESTAMP_MAX_DIFF = 15 * 60 * 1000;
    public static final int TOKEN_MAX_VALID_TIME = 15 * 24 * 60 * 60 * 1000;

    public static final String SESSION_KEY_USER_ID = "user_id";

    public static final String MESSAGE = "发布了新活动";
}
