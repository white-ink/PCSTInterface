package club.jitpcst.pcstinterface.global;

import club.jitpcst.pcstinterface.entity.User;
import club.jitpcst.pcstinterface.service.TicketService;
import club.jitpcst.pcstinterface.service.UserService;
import club.jitpcst.pcstinterface.util.SpringBeanFactoryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * 管理员拦截器，验证管理员身份
 *
 * @author 天宇小凡
 */
public class AdminInterceptor implements HandlerInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminInterceptor.class);
    private TicketService ticketService = SpringBeanFactoryUtil.getBean(TicketService.class);
    private UserService userService = SpringBeanFactoryUtil.getBean(UserService.class);

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws IOException {
        LOGGER.debug(httpServletRequest.getRequestURI());

        if (httpServletRequest.getMethod().equals(RequestMethod.OPTIONS.toString())) {
            return true;
        }

        HttpSession httpSession = httpServletRequest.getSession();
        Long userId = (Long) httpSession.getAttribute(Constants.SESSION_KEY_USER_ID);
        User user = userService.selectById(userId);
        if (user != null && user.getRole() != User.ROLE_COMMON) {
            return true;
        }
        httpServletResponse.sendError(HttpServletResponse.SC_FORBIDDEN, "your permissions are insufficient");
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {

    }
}
