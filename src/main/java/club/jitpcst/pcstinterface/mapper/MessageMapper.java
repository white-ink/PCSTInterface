package club.jitpcst.pcstinterface.mapper;

import club.jitpcst.pcstinterface.entity.Message;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 天宇小凡
 * @since 2018-07-16
 */
public interface MessageMapper extends BaseMapper<Message> {

}
