package club.jitpcst.pcstinterface.mapper;

import club.jitpcst.pcstinterface.entity.Ticket;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 天宇小凡
 * @since 2018-04-14
 */
public interface TicketMapper extends BaseMapper<Ticket> {

}
