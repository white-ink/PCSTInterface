package club.jitpcst.pcstinterface.util;

import club.jitpcst.pcstinterface.entity.User;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class RequestUtil {

    private static final String LOGIN_USER_KEY = "login-user";

    /**
     * 将请求路径转化为sqlId
     *
     * @param fullURL 完整的请求路径地址
     * @return sqlId
     */
    public static String urlToSqlId(String fullURL) {
        fullURL = fullURL.substring(fullURL.indexOf('/'), 1);
        fullURL = fullURL.substring(fullURL.indexOf('/'), 1);
        return fullURL.replaceAll("/", ".");
    }

    /**
     * 获取request中的所有请求参数，保存成map形式
     *
     * @param request 请求
     * @return hashmap
     */
    public static Map<String, Object> getRequestParamMap(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        Map<String, String[]> tempMap = request.getParameterMap();
        for (String key : tempMap.keySet()) {
            resultMap.put(key, tempMap.get(key)[0]);
        }
        return resultMap;
    }

    /**
     * 将登录的用户放入request
     *
     * @param request
     * @param user
     */
    public static void addLoginUserToRequest(HttpServletRequest request, User user) {
        request.setAttribute(LOGIN_USER_KEY, user);
    }

    /**
     * 从request中取出登录的用户
     *
     * @param request
     * @return
     */
    public static User getLoginUserFromRequest(HttpServletRequest request) {
        return (User) request.getAttribute(LOGIN_USER_KEY);
    }
}
